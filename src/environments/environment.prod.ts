export const environment = {
  production: true,
  pageSizeList:  [6, 12, 24, 48, 96],
  defaultPageSizeIndex: 0,
  xsrfHeaderName: 'X-XSRF-TOKEN',
  baseApiUrl: '/api',
  baseProductImageSource: 'assets/catalog/product/images/',
  defaultProductImage: 'assets/catalog/product/images/placeholder.jpg'
};
