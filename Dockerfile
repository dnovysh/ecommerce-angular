FROM nginx:1.22.1
COPY default.nginx.conf /etc/nginx/conf.d/default.conf
COPY dist/ecommerce-angular /usr/share/nginx/html
EXPOSE 80
